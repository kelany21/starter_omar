package helpers

import (
	"github.com/gin-gonic/gin"
	"github.com/iancoleman/strcase"
	"reflect"
	"runtime"
	"strings"
)

func Name(f func(g *gin.Context))(controllerName string, functionName string){
	path := strings.Split(runtime.FuncForPC(reflect.ValueOf(f).Pointer()).Name(), "/")
	theName := strings.Split(path[len(path)-1], ".")
	controllerName = theName[0]
	functionName = strcase.ToKebab(theName[1])
	return
}

func GET(r *gin.RouterGroup,function func(g *gin.Context), params... string) *gin.RouterGroup{
	paramsString := ""
	for _, param := range params{
		paramsString += "/:" + param
	}
	controllerName , functionName := Name(function)
	r.GET(controllerName+"/"+functionName+paramsString, function)
	return r
}

func POST(r *gin.RouterGroup,function func(g *gin.Context), params... string) *gin.RouterGroup{
	paramsString := ""
	for _, param := range params{
		paramsString += "/:" + param
	}
	controllerName , functionName := Name(function)
	r.POST(controllerName+"/"+functionName+paramsString, function)
	return r
}

func Group(r *gin.RouterGroup,function func(g *gin.Context), params... string) *gin.RouterGroup{
	paramsString := ""
	for _, param := range params{
		paramsString += "/:" + param
	}
	controllerName , functionName := Name(function)
	r.Group(controllerName+"/"+functionName+paramsString, function)
	return r
}

func DELETE(r *gin.RouterGroup,function func(g *gin.Context), params... string) *gin.RouterGroup{
	paramsString := ""
	for _, param := range params{
		paramsString += "/:" + param
	}
	controllerName , functionName := Name(function)
	r.DELETE(controllerName+"/"+functionName+paramsString, function)
	return r
}

func Any(r *gin.RouterGroup,function func(g *gin.Context), params... string) *gin.RouterGroup{
	paramsString := ""
	for _, param := range params{
		paramsString += "/:" + param
	}
	controllerName , functionName := Name(function)
	r.Any(controllerName+"/"+functionName+paramsString, function)
	return r
}

func HEAD(r *gin.RouterGroup,function func(g *gin.Context), params... string) *gin.RouterGroup{
	paramsString := ""
	for _, param := range params{
		paramsString += "/:" + param
	}
	controllerName , functionName := Name(function)
	r.HEAD(controllerName+"/"+functionName+paramsString, function)
	return r
}

func OPTIONS(r *gin.RouterGroup,function func(g *gin.Context), params... string) *gin.RouterGroup{
	paramsString := ""
	for _, param := range params{
		paramsString += "/:" + param
	}
	controllerName , functionName := Name(function)
	r.OPTIONS(controllerName+"/"+functionName+paramsString, function)
	return r
}

func PATCH(r *gin.RouterGroup,function func(g *gin.Context), params... string) *gin.RouterGroup{
	paramsString := ""
	for _, param := range params{
		paramsString += "/:" + param
	}
	controllerName , functionName := Name(function)
	r.PATCH(controllerName+"/"+functionName+paramsString, function)
	return r
}

func PUT(r *gin.RouterGroup,function func(g *gin.Context), params... string) *gin.RouterGroup{
	paramsString := ""
	for _, param := range params{
		paramsString += "/:" + param
	}
	controllerName , functionName := Name(function)
	r.PUT(controllerName+"/"+functionName+paramsString, function)
	return r
}
