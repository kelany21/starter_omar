package settings

import (
	"starter_omar/app/models"
	"starter_omar/app/requests/admin/setting"
	_const "starter_omar/const"
	"starter_omar/helpers"
)

/**
* filter module with some columns
 */
func filter() []string {
	g := _const.Services.GIN
	var filter []string
	if  g.Query("value") != ""{
		filter = append(filter, `value like "%` + g.Query("value") + `%"`)
	}
	if  g.Query("name") != ""{
		filter = append(filter, `name like "%` + g.Query("name") + `%"`)
	}
	if  g.Query("slug") != ""{
		filter = append(filter, `slug like "%` + g.Query("slug") + `%"`)
	}
	if  g.Query("setting_type") != ""{
		filter = append(filter, `setting_type like "%` + g.Query("setting_type") + `%"`)
	}
	if  g.Query("status") != ""{
		if g.Query("status") != "all"{
			filter = append(filter, `status = "` + g.Query("status") + `"`)
		}
	}
	return filter
}

/**
* preload module with some preload conditions
*/
func preload() []string {
	return []string{}
}

/**
* here we will check if request valid or not
 */
func validateRequest() (bool , *models.Setting)   {
	// init struct to validate request
	row := new(models.Setting)
	/**
	* get request and parse it to validation
	* if there any error will return with message
	 */
	err := setting.StoreUpdate(row)
	/***
	* return response if there an error if true you
	* this mean you have errors so we will return and bind data
	 */
	if helpers.ReturnNotValidRequest(err) {
		return false , row
	}
	return true , row
}

/**
* update row make sure you used UpdateOnlyAllowColumns to update allow columns
* use fill able method to only update what you need
*/
func updateColumns(data *models.Setting , oldRow *models.Setting) {
	models.Update(data, oldRow, models.SettingFillAbleColumn())
}
