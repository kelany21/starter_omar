package users

import (
	"github.com/gin-gonic/gin"
	"starter_omar/helpers"
)

/**
* all admin modules route will store here
 */
func Routes(r *gin.RouterGroup) *gin.RouterGroup  {
	helpers.GET(r, Index)
	helpers.POST(r, Store)
	helpers.PUT(r, Update, "id")
	helpers.GET(r, Show, "id")
	helpers.DELETE(r, Delete, "id")

	return r
}
