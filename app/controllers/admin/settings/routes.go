package settings

import (
	"github.com/gin-gonic/gin"
	"starter_omar/helpers"
)

/**
* all admin modules route will store here
 */
func Routes(r *gin.RouterGroup) *gin.RouterGroup  {
	helpers.GET(r, Index)
	helpers.PUT(r, Update, "id")
	helpers.GET(r, Show, "id")

	return r
}
