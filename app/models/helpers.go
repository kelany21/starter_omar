package models

import (
	_const "starter_omar/const"
)

/**
* add to active and all
 */
func IncreaseOnCreate(moduleName string) {
	IncreaseRow(_const.ACTIVE , moduleName)
	Increase("actions", "count", nil, `slug = "`+_const.ALL+`_`+moduleName+`"`)
}

/**
* remove from all and status
 */

func DecreaseOnDelete(status string , moduleName string)  {
	DecreaseRow(status , moduleName)
	Decrease("actions", "count", nil, `slug = "`+_const.ALL+`_user"`)
}

func DecreaseRow(status string , moduleName string)  {
	Decrease("actions", "count", nil, `module_name =  "`+moduleName+`"` , `verb = "`+status+`"`)
}

func IncreaseRow(status string , moduleName string)  {
	Increase("actions", "count", nil, `module_name =  "`+moduleName+`"` , `verb = "`+status+`"`)
}

func GetActionByModule(moduleName string) []Action {
	var actions []Action
	_const.Services.DB.Where("module_name = ? ", moduleName).Find(&actions)
	return actions
}

