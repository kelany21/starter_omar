package routes

import (
	"github.com/gin-gonic/gin"
	"starter_omar/app/controllers/visitor/auth"
	"starter_omar/app/controllers/visitor/general"
	"starter_omar/app/controllers/visitor/pages"
)

/***
* any route here will add after /
* anyone will have access this routes
 */
func Visitor(r *gin.RouterGroup) *gin.RouterGroup {
	general.Routes(r)
	auth.Routes(r)
	pages.Routes(r)
	/// serve static files like images
	r.Static("/public" , "./public")

	return r
}
