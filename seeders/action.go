package seeders

import (
	"starter_omar/app/models"
	_const "starter_omar/const"
)

/***
*	Seed Function must Have the same file Name then Add Seeder key word
* 	Example :  file is user function must be UserSeeder
 */
func ActionSeeder() {
	///// define default actions
	var defaultActions = make(map[string]interface{})
	/// define modules
	var modules = make(map[string]interface{})
	defaultActions["all"] = _const.ALL
	////////////// start modules have all action only
	///////////// end modules has all action only
	defaultActions["active"] =  _const.ACTIVE
	////////////// start modules have all , active actions
	modules["settings"] = defaultActions
	////////////// end modules have all , active  actions
	defaultActions["deactivate"] = _const.DEACTIVATE
	////////////// start modules have all , active , deactivate actions
	////////////// end modules have all , active , deactivate  actions
	defaultActions["trashed"] = _const.TRASH
	////////////// start modules have all , active , deactivate , trash actions
	modules["pages"] = defaultActions
	modules["faqs"] = defaultActions
	modules["categories"] = defaultActions
	////////////// end modules have all , active , deactivate , trash  actions
	defaultActions["blocked"] = _const.BLOCK
	modules["users"] = defaultActions
	/// loop to create actions
	for module , actions := range modules {
		loop := actions.(map[string]interface{})
		for noun , verb := range loop {
			newAction(models.Action{
				Noun:       noun,
				Verb:       verb.(string),
				Count:      0,
				Slug:       verb.(string) + "_" + module,
				ModuleName: module,
			})
		}
	}
}

/**
* fake data and create data base
 */
func newAction(data models.Action) {
	_const.Services.DB.Create(&data)
}
