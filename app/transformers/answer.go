package transformers

import (
	"starter_omar/app/models"
	"starter_omar/helpers"
)

/**
* stander the single Answer response
 */
func AnswerResponse(answer models.Answer) map[string]interface{} {
	var u = make(map[string]interface{})
	u["id"] = answer.ID
	u["answer"] = answer.Text
	u["created_at"] = helpers.DateFormatting(answer.CreatedAt)
	u["updated_at"] = helpers.DateFormatting(answer.UpdatedAt)

	return u
}

/**
* stander the Multi Answers response
 */
func AnswersResponse(answers []models.Answer) []map[string]interface{} {
	var u  = make([]map[string]interface{} , 0)
	for _ , answer := range answers {
		u = append(u , AnswerResponse(answer))
	}
	return u
}
