package pages

import (
	"github.com/gin-gonic/gin"
	"github.com/iancoleman/strcase"
	"github.com/jinzhu/gorm"
	"starter_omar/app/models"
	"starter_omar/app/transformers"
	_const "starter_omar/const"
	"starter_omar/helpers"
)

const controllerName  = "pages"
/***
* get all rows with pagination
 */
func Index(g *gin.Context) {
	_const.Services.GIN = g
	// array of rows
	var rows []models.Page
	// query before any thing
	paginator := helpers.Paging(&helpers.Param{
		DB:      _const.Services.DB,
		Page:    helpers.Page(),
		Limit:   helpers.Limit(),
		OrderBy: helpers.Order("id desc"),
		Filters: filter(),
		Preload: preload(),
		ShowSQL: true,
	}, &rows)
	// transform slice
	response := make(map[string]interface{})
	response["status"] = transformers.ActionsResponse(models.GetActionByModule("pages"))
	response["data"] = transformers.PagesResponse(rows)
	// transform slice
	paginator.Records = response
	// return response
	helpers.OkResponseWithPaging(helpers.DoneGetAllItems(), paginator)
}

/***
* return row with id
 */
func Show(g *gin.Context) {
	_const.Services.GIN = g
	// find this row or return 404
	var row models.Page
	// check if this id exits , abort if not
	if models.FindOrFail(g.Param("id"), &row , (func(db *gorm.DB) {
		db.Preload("Translations" , "lang = ?", helpers.LangHeader())
	}) , (func(db *gorm.DB) {
		db.Preload("Images")
	})); row.ID == 0 {
		return
	}
	// now return row data after transformers
	helpers.OkResponse(helpers.DoneGetItem(), transformers.PageResponse(row))
}

/**
* update category
 */
func Update(g *gin.Context) {
	_const.Services.GIN = g
	// get language
	lang := helpers.LangHeader()
	// check if request valid
	valid, row := validateRequest()
	if !valid {
		return
	}
	// find this row or return 404
	var oldRow models.Page
	// check if this id exits , abort if not
	if models.FindOrFail(g.Param("id"), &oldRow ); oldRow.ID == 0 {
		return
	}
	/// delete all images if reset flag in the url
	if g.Query("reset") == "true" {
		deleteAllPageImage(oldRow.ID)
	}
	/// upload images
	insertImageInDataBase(row.Image, int(oldRow.ID))
	/// insert translations
	insertTranslationsInDataBase(row.Translation, int(oldRow.ID))
	/// update allow columns
	updateColumns(row, &oldRow, lang)
	// now return row data after transformers
	helpers.OkResponse(helpers.DoneUpdate(), transformers.PageResponse(oldRow))
}

/***
* loop on translation
* insert in database
 */
func insertTranslationsInDataBase(translations []map[string]string, id int) {
	if len(translations) > 0 {
		for _, translation := range translations {
			_const.Services.DB.Create(&models.Translation{
				PageId: id,
				Value:  translation["value"],
				Slug:   strcase.ToSnake(translation["slug"]),
				Lang:   translation["lang"],
			})
		}
	}
}
