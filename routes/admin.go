package routes

import (
	"github.com/gin-gonic/gin"
	"starter_omar/app/controllers/admin/Pages"
	"starter_omar/app/controllers/admin/categories"
	"starter_omar/app/controllers/admin/faqs"
	"starter_omar/app/controllers/admin/settings"
	"starter_omar/app/controllers/admin/translations"
	"starter_omar/app/controllers/admin/users"
)

/***
* any route here will add after /admin
* admin only  will have access this routes
 */
func Admin(r *gin.RouterGroup) *gin.RouterGroup {
	categories.Routes(r)
	translations.Routes(r)
	settings.Routes(r)
	users.Routes(r)
	pages.Routes(r)
	faqs.Routes(r)

	return r
}
