package pages

import (
	"github.com/gin-gonic/gin"
	"starter_omar/app/models"
	"starter_omar/app/transformers"
	_const "starter_omar/const"
	"starter_omar/helpers"
)

const controllerName  = "pages"

/**
* get faq with answers
*/
func Faqs(g *gin.Context) {
	_const.Services.GIN = g
	///// declare variables
	var rows []models.Faq
	_const.Services.DB.Scopes(models.ActiveFaq).Preload("Answers").Find(&rows)
	/// now return row data after transformers
	helpers.OkResponse( helpers.DoneGetItem(), transformers.FaqsResponse(rows))
}
