package _const

import (
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"net/http"
)

var Services *Service;

/**
* Services instructor
 */
type Service struct {
	GIN *gin.Context
	DB  *gorm.DB
	DBERR error
}

/**
* Init Services
 */
func ServicesInit() *Service  {
	return &Service{}
}

/**
* short hand to get request
 */
func Request() *http.Request  {
	return 	Services.GIN.Request
}