package transformers

import (
	"starter_omar/app/models"
	"starter_omar/helpers"
)

/**
* stander the single faq response
 */
func FaqResponse(faq models.Faq) map[string]interface{} {
	var u = make(map[string]interface{})
	u["question"] = faq.Question
	u["id"] = faq.ID
	u["status"] = faq.Status
	u["answer"] = AnswersResponse(faq.Answers)
	u["created_at"] = helpers.DateFormatting(faq.CreatedAt)
	u["updated_at"] = helpers.DateFormatting(faq.UpdatedAt)

	return u
}

/**
* stander the Multi faqs response
 */
func FaqsResponse(faqs []models.Faq) []map[string]interface{} {
	var u = make([]map[string]interface{}, 0)
	for _, faq := range faqs {
		u = append(u, FaqResponse(faq))
	}
	return u
}
