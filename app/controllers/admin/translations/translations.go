package translations

import (
	"github.com/gin-gonic/gin"
	"starter_omar/app/models"
	"starter_omar/app/transformers"
	_const "starter_omar/const"
	"starter_omar/helpers"
)


const controllerName  = "translations"
/***
* get all rows with pagination
 */
func Index(g *gin.Context) {
	_const.Services.GIN = g
	// array of rows
	var rows []models.Translation
	// query before any thing
	paginator := helpers.Paging(&helpers.Param{
		DB:      _const.Services.DB,
		Page:    helpers.Page(),
		Limit:   helpers.Limit(),
		OrderBy: helpers.Order("id desc"),
		Filters: filter(),
		Preload: preload(),
		ShowSQL: true,
	}, &rows)
	// transform slice
	paginator.Records = transformers.TranslationsResponse(rows)
	// return response
	helpers.OkResponseWithPaging(helpers.DoneGetAllItems(), paginator)
}
/***
* return row with id
 */
func Show(g *gin.Context) {
	_const.Services.GIN = g
	// find this row or return 404
	var row models.Translation
	// check if this id exits , abort if not
	if models.FindOrFail(g.Param("id"), &row); row.ID == 0 {
		return
	}
	// now return row data after transformers
	helpers.OkResponse(helpers.DoneGetItem(), transformers.TranslationResponse(row))
}

/**
* update category
 */
func Update(g *gin.Context) {
	// check if request valid
	valid, row := validateRequest()
	if !valid {
		return
	}
	// find this row or return 404
	var oldRow models.Translation
	// check if this id exits , abort if not
	if models.FindOrFail(g.Param("id"), &oldRow); oldRow.ID == 0 {
		return
	}
	// check if page exists
	if row.PageId != 0 {
		var page models.Page
		// check if this id exits , abort if not
		if models.FindOrFail(row.PageId, &page); page.ID == 0 {
			return
		}
	}
	/// update allow columns
	updateColumns(row, &oldRow)
	// now return row data after transformers
	helpers.OkResponse(helpers.DoneUpdate(), transformers.TranslationResponse(oldRow))
}
