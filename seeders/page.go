package seeders

import (
	"starter_omar/app/models"
	_const "starter_omar/const"
)

/***
*	Seed Function must Have the same file Name then Add Seeder key word
* 	Example :  file is user function must be UserSeeder
 */
func  PageSeeder() {
	for _, page := range pages() {
		data := models.Page{
			Name:   page,
			Status: _const.ACTIVE,
		}
		_const.Services.DB.Create(&data)
	}
}

/***
* list of pages
 */
func pages() []string {
	return []string{
		"home",
		"about",
		"contact",
		"terms",
		"police",
	}
}
