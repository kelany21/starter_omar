package faqs

import (
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"starter_omar/app/models"
	"starter_omar/app/transformers"
	_const "starter_omar/const"
	"starter_omar/helpers"
)

const controllerName  = "faqs"
/***
* get all rows with pagination
 */
func Index(g *gin.Context) {
	_const.Services.GIN = g
	/// array of rows
	var rows []models.Faq
	/// query before any thing
	paginator := helpers.Paging(&helpers.Param{
		DB:      _const.Services.DB,
		Page:    helpers.Page(),
		Limit:   helpers.Limit(),
		OrderBy: helpers.Order("id desc"),
		Filters: filter(),
		Preload: preload(),
		ShowSQL: true,
	}, &rows)
	/// transform slice
	response := make(map[string]interface{})
	response["status"] = transformers.ActionsResponse(models.GetActionByModule("faqs"))
	response["data"] = transformers.FaqsResponse(rows)
	// transform slice
	paginator.Records = response
	/// return response
	helpers.OkResponseWithPaging(helpers.DoneGetAllItems(), paginator)
}

/**
* store new category
 */
func Store(g *gin.Context) {
	_const.Services.GIN = g
	/// check if request valid
	valid, row := validateRequest()
	if !valid {
		return
	}
	/// create new row
	_const.Services.DB.Create(&row)
	/// insert in to answer
	insertAnswerToDataBase(row.Answer, row.ID)
	/// get with new data
	models.FindOrFail(g.Param("id"), &row, (func(db *gorm.DB) {
		db.Preload("Answers")
	}))
	/// now return row data after transformers
	helpers.OkResponse(helpers.DoneCreateItem(), transformers.FaqResponse(*row))
}

/***
* return row with id
 */
func Show(g *gin.Context) {
	_const.Services.GIN = g
	var row models.Faq
	// check if this id exits , abort if not
	if models.FindOrFail(g.Param("id"), &row); row.ID == 0 {
		return
	}
	/// now return row data after transformers
	helpers.OkResponse(helpers.DoneGetItem(), transformers.FaqResponse(row))
}

/***
* delete row with id
 */
func Delete(g *gin.Context) {
	_const.Services.GIN = g
	/// find this row or return 404
	var row models.Faq
	// check if this id exits , abort if not
	if models.FindOrFail(g.Param("id"), &row); row.ID == 0 {
		return
	}
	/// delete related answers
	deleteAnswers(row.ID)
	/// delete row
	_const.Services.DB.Unscoped().Delete(&row)
	/// now return ok response
	helpers.OkResponseWithOutData(helpers.DoneDelete())
}

/**
* update category
 */
func Update(g *gin.Context) {
	_const.Services.GIN = g
	/// check if request valid
	valid, row := validateRequest()
	if !valid {
		return
	}
	/// find this row or return 404
	var oldRow models.Faq
	// check if this id exits , abort if not
	if models.FindOrFail(g.Param("id"), &row); row.ID == 0 {
		return
	}
	/// delete old Answer if user need
	if g.Query("reset") == "true" {
		deleteAnswers(oldRow.ID)
	}
	/// insert in to answer
	insertAnswerToDataBase(row.Answer, oldRow.ID)
	/// update allow columns
	updateColumns(row, &oldRow)
	/// now return row data after transformers
	helpers.OkResponse(helpers.DoneUpdate(), transformers.FaqResponse(oldRow))
}
