package general

import (
	"github.com/gin-gonic/gin"
	"starter_omar/app/models"
	"starter_omar/app/transformers"
	_const "starter_omar/const"
	"starter_omar/helpers"
)

const controllerName  = "general"

/***
* this will be the first api
* call in the hole system
* return all translations
* all setting
 */
//TODO::will cache this response in redis
func Init(g *gin.Context) {
	_const.Services.GIN = g
	/// declare variables
	var settings []models.Setting
	var pages []models.Page
	/// queries
	_const.Services.DB.Preload("Translations").Preload("Images").Find(&pages)
	_const.Services.DB.Find(&settings)
	/// build response
	var response = make(map[string]interface{})
	response["pages"] = transformers.PagesResponse(pages)
	response["settings"] = transformers.SettingsResponse(settings)
	/// return with data
	helpers.OkResponse(helpers.T("init_project"), response)
	return
}
