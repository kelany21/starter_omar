package config

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"github.com/subosito/gotenv"
	"os"
	_const "starter_omar/const"
	"strconv"
)

/**
* connect with data base with env file params
* just edit all data in .env file
 */
func ConnectToDatabase() {
	_const.Services.DBERR = gotenv.Load()
	if _const.Services.DBERR != nil {
		fmt.Println(_const.Services.DBERR)
	}
	if _const.Services.DB == nil {
		_const.Services.DB, _const.Services.DBERR = gorm.Open("mysql", os.Getenv("DATABASE_USERNAME")+":"+os.Getenv("DATABASE_PASSWORD")+"@tcp("+os.Getenv("DATABASE_HOST")+":"+os.Getenv("DATABASE_PORT")+")/"+os.Getenv("DATABASE_NAME")+"?charset=utf8&parseTime=True&loc=Local")
	}
	debug, _ := strconv.ParseBool(os.Getenv("DEBUG_DATABASE"))
	if os.Getenv("APP_ENV") == "local" {
		_const.Services.DB.LogMode(debug)
	}
}
