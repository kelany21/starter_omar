package categories

import (
	"github.com/gin-gonic/gin"
	"starter_omar/app/models"
	"starter_omar/app/transformers"
	_const "starter_omar/const"
	"starter_omar/helpers"
)

/***
* get all rows with pagination
 */
func Index(g *gin.Context) {
	_const.Services.GIN = g
	// array of rows
	var rows []models.Category
	// query before any thing
	paginator := helpers.Paging(&helpers.Param{
		DB:      _const.Services.DB,
		Page:    helpers.Page(),
		Limit:   helpers.Limit(),
		OrderBy: helpers.Order("id desc"),
		Filters: filter(),
		Preload: preload(),
		ShowSQL: true,
	}, &rows)
	// transform slice
	response := make(map[string]interface{})
	response["status"] = transformers.ActionsResponse(models.GetActionByModule("categories"))
	response["data"] = transformers.CategoriesResponse(rows)
	// transform slice
	paginator.Records = response
	// return response
	helpers.OkResponseWithPaging(helpers.DoneGetAllItems(), paginator)
}

/**
* store new category
 */
func Store(g *gin.Context) {
	_const.Services.GIN = g
	// check if request valid
	valid, row := validateRequest()
	if !valid {
		return
	}
	// create new row
	_const.Services.DB.Create(&row)
	//now return row data after transformers
	helpers.OkResponse(helpers.DoneCreateItem(), transformers.CategoryResponse(*row))
}

/***
* return row with id
 */
func Show(g *gin.Context) {
	_const.Services.GIN = g
	var row models.Category
	// check if this id exits , abort if not
	if models.FindOrFail(g.Param("id"), &row); row.ID == 0 {
		return
	}
	// now return row data after transformers
	helpers.OkResponse(helpers.DoneGetItem(), transformers.CategoryResponse(row))
}

/***
* delete row with id
 */
func Delete(g *gin.Context) {
	_const.Services.GIN = g
	var row models.Category
	// check if this id exits , abort if not
	if models.FindOrFail(g.Param("id"), &row); row.ID == 0 {
		return
	}
	_const.Services.DB.Unscoped().Delete(&row)
	// now return ok response
	helpers.OkResponseWithOutData(helpers.DoneDelete())
}

/**
* update category
 */
func Update(g *gin.Context) {
	_const.Services.GIN = g
	// check if request valid
	valid, row := validateRequest()
	if !valid {
		return
	}
	// find this row or return 404
	var oldRow models.Category
	// check if this id exits , abort if not
	if models.FindOrFail(g.Param("id"), &oldRow); oldRow.ID == 0 {
		return
	}
	/// update allow columns
	updateColumns(row, &oldRow)
	// now return row data after transformers
	helpers.OkResponse(helpers.DoneUpdate(), transformers.CategoryResponse(oldRow))
}
