package auth

import (
	"github.com/gin-gonic/gin"
	"starter_omar/helpers"
)

/**
* all admin modules route will store here
 */
func Routes(r *gin.RouterGroup) *gin.RouterGroup {
	/// start not auth routes
	helpers.POST(r, Login)
	helpers.POST(r, Register)
	helpers.POST(r, Reset)
	helpers.POST(r, Recover)

	return r
}
