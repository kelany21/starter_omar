package settings

import (
	"github.com/gin-gonic/gin"
	"starter_omar/app/models"
	"starter_omar/app/transformers"
	_const "starter_omar/const"
	"starter_omar/helpers"
)

const controllerName  = "settings"
/***
* get all rows with pagination
 */
func Index(g *gin.Context) {
	_const.Services.GIN = g
	// array of rows
	var rows []models.Setting
	// query before any thing
	paginator := helpers.Paging(&helpers.Param{
		DB:      _const.Services.DB,
		Page:    helpers.Page(),
		Limit:   helpers.Limit(),
		OrderBy: helpers.Order("id desc"),
		Filters: filter(),
		Preload: preload(),
		ShowSQL: true,
	}, &rows)
	// transform slice
	response := make(map[string]interface{})
	response["status"] = transformers.ActionsResponse(models.GetActionByModule("settings"))
	response["data"] = transformers.SettingsResponse(rows)
	// transform slice
	paginator.Records = response
	// return response
	helpers.OkResponseWithPaging(helpers.DoneGetAllItems(), paginator)
}

/***
* return row with id
 */
func Show(g *gin.Context) {
	_const.Services.GIN = g
	// find this row or return 404
	var row models.Setting
	// check if this id exits , abort if not
	if models.FindOrFail(g.Param("id"), &row); row.ID == 0 {
		return
	}
	// now return row data after transformers
	helpers.OkResponse(helpers.DoneGetItem(), transformers.SettingResponse(row))
}

/**
* update category
 */
func Update(g *gin.Context) {
	_const.Services.GIN = g
	// check if request valid
	valid, row := validateRequest()
	if !valid {
		return
	}
	// find this row or return 404
	var oldRow models.Setting
	// check if this id exits , abort if not
	if models.FindOrFail(g.Param("id"), &row); row.ID == 0 {
		return
	}
	/// update allow columns
	updateColumns(row, &oldRow)
	// now return row data after transformers
	helpers.OkResponse( helpers.DoneUpdate(), transformers.SettingResponse(oldRow))
}
