package pages

import (
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"starter_omar/app/models"
	"starter_omar/app/requests/admin/page"
	"starter_omar/app/transformers"
	_const "starter_omar/const"
	"starter_omar/helpers"
	"strconv"
)

/**
* update image form page
 */
func UploadImage(g *gin.Context) {
	// init struct to validate request
	var row models.PageImageRequest
	/**
	* get request and parse it to validation
	* if there any error will return with message
	 */
	err := page.UploadStoreUpdate(&row)
	/***
	* return response if there an error if true you
	* this mean you have errors so we will return and bind data
	 */
	if helpers.ReturnNotValidRequest(err) {
		return
	}
	///get id
	id, _ := strconv.Atoi(g.Param("id"))
	// find this row or return 404
	var page models.Page
	// check if this id exits , abort if not
	if models.FindOrFail(g.Param("id"), &page); page.ID == 0 {
		return
	}
	/// upload images and insert in database
	insertImageInDataBase(row.Images, id)
	/// get the new data with images
	var newPage models.Page
	if models.FindOrFail(g.Param("id"), &row , (func(db *gorm.DB) {
		db.Preload("Translations" , "lang = ?", helpers.LangHeader())
	}) , (func(db *gorm.DB) {
		db.Preload("Images")
	})); newPage.ID == 0 {
		return
	}
	helpers.OkResponse(helpers.DoneUpdate(), transformers.PageResponse(newPage))
}

/***
* Delete images
* Delete image by id
 */
func DeleteImage(g *gin.Context) {
	// find this row or return 404
	var row models.PageImage
	// check if this id exits , abort if not
	if models.FindOrFail(g.Param("id"), &row); row.ID == 0 {
		return
	}
	_const.Services.DB.Unscoped().Delete(&row)
	// now return ok response
	helpers.OkResponseWithOutData(helpers.DoneDelete())
}

/***
* Delete images assign to page by page id
 */
func DeletePageImages(g *gin.Context) {
	// find this row or return 404
	var row models.Page
	// check if this id exits , abort if not
	if models.FindOrFail(g.Param("id"), &row); row.ID == 0 {
		return
	}
	deleteAllPageImage(row.ID)
	// now return ok response
	helpers.OkResponseWithOutData(helpers.DoneDelete())
	return
}

/***
*  get all page image and delete
 */
func deleteAllPageImage(id uint) {
	var rows models.PageImage
	_const.Services.DB.Unscoped().Where("page_id = ? ", id).Delete(&rows)
}

/**
* upload images
* loop and insert images with id
 */
func insertImageInDataBase(images []string, id int) {
	if len(images) > 0 {
		uploadedImages := helpers.MultiDecodeImage(images)
		///// loop and insert image in database
		if len(uploadedImages) > 0 {
			for _, upload := range uploadedImages {
				_const.Services.DB.Create(&models.PageImage{
					PageId: id,
					Image:  upload,
				})
			}
		}
	}
}
