package helpers

import (
	"github.com/bykovme/gotrans"
	"github.com/gin-gonic/gin"
	"github.com/thedevsaddam/govalidator"
	"net/http"
	_const "starter_omar/const"
)

/**
* conflict
 */
func ReturnBadRequest() {
	var errors map[string]string
	var data map[string]interface{}
	var msg = gotrans.Tr(GetCurrentLang(), "400")
	response( msg, data, errors, 400, 400, false)
	return
}

/**
* Duplicate data
 */
func ReturnDuplicateData(inputName string) {
	var errors map[string]string
	var data map[string]interface{}
	var msg = T("duplicate_data_part_one", inputName, "duplicate_data_part_two")
	response(msg, data, errors, 409, 409, false)
	return
}

/**
* upload error
 */
func UploadError() {
	var errors map[string]string
	var data map[string]interface{}
	var msg = T("upload_error_code")
	response(msg, data, errors, 415, 415, false)
	return
}

/**
* multi upload error
 */
func MultiUploadError() {
	var errors map[string]string
	var data map[string]interface{}
	var msg = T("upload_multi_images_error_code")
	response(msg, data, errors, 415, 415, false)
	return
}

/**
* NotValidRequest response
 */
func ReturnNotValidRequest(error *govalidator.Validator) bool {
	e := error.ValidateJSON()
	if len(e) > 0 {
		_const.Services.GIN.JSON(
			http.StatusBadRequest, gin.H{
				"status":  false,
				"message": gotrans.Tr(GetCurrentLang(), "400"),
				"errors":  e,
				"code":    400,
				"payload": nil,
			})
		return true
	}
	return false
}

/**
* NotValidFile response
 */
func ReturnNotValidRequestFile(error *govalidator.Validator) bool {
	e := error.Validate()
	if len(e) > 0 {
		_const.Services.GIN.JSON(
			http.StatusBadRequest, gin.H{
				"status":  false,
				"message": gotrans.Tr(GetCurrentLang(), "400"),
				"errors":  e,
				"code":    400,
				"payload": nil,
			})
		return true
	}
	return false
}

/**
* NotValidRequest response
 */
func ReturnNotValidRequestFormData(error *govalidator.Validator) bool {
	e := error.Validate()
	if len(e) > 0 {
		_const.Services.GIN.JSON(
			http.StatusBadRequest, gin.H{
				"status":  false,
				"message": gotrans.Tr(GetCurrentLang(), "400"),
				"errors":  e,
				"code":    400,
				"payload": nil,
			})
		return true
	}
	return false
}

/**
* NotFound response
 */
func ReturnNotFound(msg string) {
	var errors map[string]string
	var data map[string]interface{}
	response(msg, data, errors, http.StatusNotFound, 404, false)
	return
}

/**
* Forbidden response
 */
func ReturnForbidden(msg string) {
	var errors map[string]string
	var data map[string]interface{}
	response(msg, data, errors, http.StatusForbidden, 403, false)
	return
}

/**
* ok response with data
 */
func OkResponse(msg string, data interface{}) {
	var errors map[string]string
	response(msg, data, errors, http.StatusOK, 200, true)
	return
}

/**
* ok response without data
 */
func OkResponseWithOutData(msg string) {
	var errors map[string]string
	var data map[string]interface{}
	response(msg, data, errors, http.StatusOK, 200, true)
	return
}

/**
* Not Authorize
 */
func ReturnYouAreNotAuthorize() {
	var errors map[string]string
	var data map[string]interface{}
	var msg = gotrans.Tr(GetCurrentLang(), "401")
	response(msg, data, errors, 401, 401, true)
	return
}

/**
* ok with paging
 */
func OkResponseWithPaging(msg string, data *Paginator) {
	var errors map[string]string
	response(msg, data, errors, http.StatusOK, 200, true)
	return
}

/**
* stander response
 */
func response(msg string, data interface{}, errors map[string]string, httpStatus int, code int, status bool) {
	_const.Services.GIN.JSON(httpStatus, gin.H{
		"status":  status,
		"message": msg,
		"errors":  errors,
		"code":    code,
		"payload": data,
	})
	return
}

/**
* NotValidRequest file
 */
func ReturnNotValidFile(err error) {
	_const.Services.GIN.JSON(
		http.StatusBadRequest, gin.H{
			"status":  false,
			"message": gotrans.Tr(GetCurrentLang(), "400"),
			"errors":  err,
			"code":    400,
			"payload": nil,
		})
}

/**
*  global response
 */
func ReturnResponseWithMessageAndStatus(statusHttp int, message string, status bool) {
	var errors map[string]string
	var data map[string]interface{}
	var msg = gotrans.Tr(GetCurrentLang(), message)
	response(msg, data, errors, statusHttp, statusHttp, status)
	return
}
