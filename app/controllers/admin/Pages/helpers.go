package pages

import (
	"starter_omar/app/models"
	"starter_omar/app/requests/admin/page"
	_const "starter_omar/const"
	"starter_omar/helpers"
)

/**
* filter module with some columns
 */
func filter() []string {
	g := _const.Services.GIN
	var filter []string
	if g.Query("status") != "" {
		filter = append(filter, "status = "+g.Query("status"))
	}
	if g.Query("name") != "" {
		filter = append(filter, `name like "%`+g.Query("name")+`%"`)
	}
	if g.Query("status") != "" {
		if g.Query("status") != "all" {
			filter = append(filter, `status = "`+g.Query("status")+`"`)
		}
	}
	return filter
}

/**
* preload module with some preload conditions
 */
func preload() []string {
	return []string{}
}

/**
* preload function when findOrFail
 */

/**
* here we will check if request valid or not
 */
func validateRequest() (bool, *models.Page) {
	// init struct to validate request
	row := new(models.Page)
	/**
	* get request and parse it to validation
	* if there any error will return with message
	 */
	err := page.StoreUpdate(row)
	/***
	* return response if there an error if true you
	* this mean you have errors so we will return and bind data
	 */
	if helpers.ReturnNotValidRequest(err) {
		return false, row
	}
	return true, row
}

func FindOrFailWithPreload(id interface{}, lang string) (models.Page, bool) {
	var oldRow models.Page
	db := _const.Services.DB.Where("id = ? ", id)
	// if user change language will get the new language keys
	db = db.Preload("Translations", "lang = ?", lang)
	// preload
	db = helpers.PreloadD(db, []string{"Images"})
	db.Find(&oldRow)
	if oldRow.ID != 0 {
		return oldRow, true
	}
	return oldRow, false
}

/**
* update row make sure you used UpdateOnlyAllowColumns to update allow columns
* use fill able method to only update what you need
 */
func updateColumns(data *models.Page, oldRow *models.Page, lang string) {
	models.Update(data, oldRow, models.PageFillAbleColumn(), "Translations", "Images")
}
