package helpers

import (
	"fmt"
	"github.com/bykovme/gotrans"
	"github.com/gin-gonic/gin"
	"strconv"
	"time"
)

func DateFormatting(time time.Time) string {
	if time.Day() < 10 {
		return strconv.Itoa(time.Year()) + "-" + MonthNumber(time.Month()) + "-" + "0" + strconv.Itoa(time.Day())
	} else {
		return strconv.Itoa(time.Year()) + "-" + MonthNumber(time.Month()) + "-" + strconv.Itoa(time.Day())
	}
}

func TimeFormatting(time time.Time) string {
	hour, minute, _ := time.Clock()
	hourString := ""
	minuteString := ""
	status := gotrans.Tr(GetCurrentLang(), "AM")
	if hour > 11 {
		status = gotrans.Tr(GetCurrentLang(), "PM")
	}
	if hour > 12 {
		hour %= 12
	}
	if hour < 10 {
		hourString = "0" + strconv.Itoa(hour)
	} else {
		hourString = strconv.Itoa(hour)
	}
	if minute < 10 {
		minuteString = "0" + strconv.Itoa(minute)
	} else {
		minuteString = strconv.Itoa(minute)
	}
	return hourString + " : " + minuteString + " " + status
}

func WeekDayNumber(weekday time.Weekday) int {
	var weekDayNumber = make(map[string]int)
	weekDayNumber["Sunday"] = 0
	weekDayNumber["Monday"] = 1
	weekDayNumber["Tuesday"] = 2
	weekDayNumber["Wednesday"] = 3
	weekDayNumber["Thursday"] = 4
	weekDayNumber["Friday"] = 5
	weekDayNumber["Saturday"] = 6
	return weekDayNumber[weekday.String()]
}

func MonthNumber(month time.Month) string {
	var monthNumber = make(map[string]string)
	monthNumber["January"] = "01"
	monthNumber["February"] = "02"
	monthNumber["March"] = "03"
	monthNumber["April"] = "04"
	monthNumber["May"] = "05"
	monthNumber["June"] = "06"
	monthNumber["July"] = "07"
	monthNumber["August"] = "08"
	monthNumber["September"] = "09"
	monthNumber["October"] = "10"
	monthNumber["November"] = "11"
	monthNumber["December"] = "12"
	return monthNumber[month.String()]
}

func MonthDayNumber(month time.Month, leap bool) int {
	var monthNumber = make(map[string]int)
	monthNumber["January"] = 31
	monthNumber["February"] = 28
	monthNumber["March"] = 31
	monthNumber["April"] = 30
	monthNumber["May"] = 31
	monthNumber["June"] = 30
	monthNumber["July"] = 31
	monthNumber["August"] = 31
	monthNumber["September"] = 30
	monthNumber["October"] = 31
	monthNumber["November"] = 30
	monthNumber["December"] = 31
	if leap {
		monthNumber["February"] = 29
	}
	return monthNumber[month.String()]
}

func LeapYear(year int) bool {
	if year%4 == 0 {
		if year%100 == 0 {
			if year%400 == 0 {
				return true
			}
			return false
		}
		return true
	}
	return false
}

func ConvertToTime(timeString string) (timeStamp time.Time, err error) {
	layOut := "Mon Jan 02 15:04:05 -0700 2006"
	timeStamp, err = time.Parse(layOut, timeString)
	return
}

func ConvertTimeToArabic(g *gin.Context, date time.Time) string {
	if date.IsZero() {
		date = time.Now()
	}
	//date.ToLocaleDateString('ar-EG-u-nu-latn',{weekday: 'long', year: 'numeric', month: 'short', day: 'numeric'});
	if g.GetHeader("Accept-Language") == "ar" {
		months := []string{"يناير", "فبراير", "مارس", "إبريل", "مايو", "يونيو",
			"يوليو", "أغسطس", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر",
		}
		d := fmt.Sprintf("%d %s %d",
			date.Day(), months[date.Month()-1], date.Year())
		fmt.Println(d)
		return d
	} else {
		return DateFormatting(date)
	}

}