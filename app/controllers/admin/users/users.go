package users

import (
	"github.com/gin-gonic/gin"
	"starter_omar/app/models"
	"starter_omar/app/transformers"
	_const "starter_omar/const"
	"starter_omar/helpers"
)

const controllerName  = "users"
/***
* get all rows with pagination
 */
func Index(g *gin.Context) {
	_const.Services.GIN = g
	// array of rows
	var rows []models.User
	// query before any thing
	paginator := helpers.Paging(&helpers.Param{
		DB:      _const.Services.DB,
		Page:    helpers.Page(),
		Limit:   helpers.Limit(),
		OrderBy: helpers.Order("id desc"),
		Filters: filter(),
		Preload: preload(),
		ShowSQL: true,
	}, &rows)
	response := make(map[string]interface{})
	response["status"] = transformers.ActionsResponse(models.GetActionByModule("users"))
	response["data"] = transformers.UsersResponse(rows)
	// transform slice
	paginator.Records = response
	// return response
	helpers.OkResponseWithPaging(helpers.DoneGetAllItems(), paginator)
}

/**
* store new category
 */
func Store(g *gin.Context) {
	_const.Services.GIN = g
	// check if request valid
	valid, row := validateRequest("store")
	if !valid {
		return
	}
	/// check if this email exists
	var count int
	_const.Services.DB.Model(models.User{}).Where("email = ? ", row.Email).Count(&count)
	if count > 0 {
		helpers.ReturnDuplicateData("email")
		return
	}
	// create new row
	_const.Services.DB.Create(&row)
	//now return row data after transformers
	helpers.OkResponse(helpers.DoneCreateItem(), transformers.UserResponse(*row))
}

/***
* return row with id
 */
func Show(g *gin.Context) {
	_const.Services.GIN = g
	var row models.User
	// check if this id exits , abort if not
	if models.FindOrFail(g.Param("id"), &row); row.ID == 0 {
		return
	}
	// now return row data after transformers
	helpers.OkResponse(helpers.DoneGetItem(), transformers.UserResponse(row))
}

/***
* delete row with id
 */
func Delete(g *gin.Context) {
	_const.Services.GIN = g
	var row models.User
	// check if this id exits , abort if not
	if models.FindOrFail(g.Param("id"), &row); row.ID == 0 {
		return
	}
	_const.Services.DB.Unscoped().Delete(&row)
	// now return row data after transformers
	helpers.OkResponseWithOutData(helpers.DoneDelete())
}

/**
* update category
 */
func Update(g *gin.Context) {
	_const.Services.GIN = g
	// check if request valid
	valid, data := validateRequest("update")
	if !valid {
		return
	}
	// find this row or return 404
	var oldRow models.User
	// check if this id exits , abort if not
	if models.FindOrFail(g.Param("id"), &oldRow); oldRow.ID == 0 {
		return
	}
	/// check if this email exists
	var count int
	_const.Services.DB.Model(models.User{}).Where("email = ? AND email != ?", data.Email, oldRow.Email).Count(&count)
	if count > 0 {
		helpers.ReturnDuplicateData("email")
		return
	}
	/// update allow columns
	updateColumns(data, &oldRow)
	// now return row data after transformers
	helpers.OkResponse(helpers.DoneUpdate(), transformers.UserResponse(oldRow))
}
