package faqs

import (
	"starter_omar/app/models"
	_const "starter_omar/const"
	"starter_omar/helpers"
)

/**
* loop on the answers
* insert all answer in database
*/
func insertAnswerToDataBase(answers []string, id uint) {
	if len(answers) > 0 {
		for _, answer := range answers {
			if answer != ""{
				_const.Services.DB.Create(&models.Answer{
					Text: helpers.ClearText(answer),
					FaqId:int(id),
				})
			}
		}
	}
}

/**
*  delete answers
*/
func deleteAnswers(faqId uint)  {
	var rows models.Answer
	_const.Services.DB.Unscoped().Where("faq_id = ? ", faqId).Delete(&rows)
}
