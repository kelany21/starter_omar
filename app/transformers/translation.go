package transformers

import (
	"starter_omar/app/models"
	"starter_omar/helpers"
)

/**
* stander the single translation response
 */
func TranslationResponse(translation models.Translation) map[string]interface{} {
	var u = make(map[string]interface{})
	u["value"] = translation.Value
	u["id"] = translation.ID
	u["slug"] = translation.Slug
	u["lang"] = translation.Lang
	u["page_id"] = translation.PageId
	u["created_at"] = helpers.DateFormatting(translation.CreatedAt)
	u["updated_at"] = helpers.DateFormatting(translation.UpdatedAt)

	return u
}

/**
* stander the Multi translations response
 */
func TranslationsResponse(translations []models.Translation) []map[string]interface{} {
	var u = make([]map[string]interface{}, 0)
	for _, translation := range translations {
		u = append(u, TranslationResponse(translation))
	}

	return u
}
