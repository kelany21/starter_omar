package faq

import (
	"github.com/thedevsaddam/govalidator"
	"starter_omar/app/models"
	_const "starter_omar/const"
	"starter_omar/helpers"
)

/**
* validate store faq request
 */
func StoreUpdate(request *models.Faq) *govalidator.Validator {
	lang := helpers.GetCurrentLangFromHttp()
	/// Validation rules
	rules := govalidator.MapData{
		"question": []string{"required", "min:6", "max:225"},
		"status":   []string{"required", "between:1,2"},
		"answer":   []string{"strings_slice"},
	}

	messages := govalidator.MapData{
		"question": []string{helpers.Required(lang), helpers.Min(lang, "6"), helpers.Max(lang, "50")},
		"status":   []string{helpers.Required(lang), helpers.Between(lang, "1,2")},
		"answer":   []string{helpers.StringsSlice(lang)},
	}

	opts := govalidator.Options{
		Request:         _const.Request(),      // request object
		Rules:           rules, // rules map
		Data:            request,
		Messages:        messages, // custom message map (Optional)
		RequiredDefault: true,     // all the field to be pass the rules
	}
	return govalidator.New(opts)
}
