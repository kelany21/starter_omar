package general

import (
	"github.com/gin-gonic/gin"
	"starter_omar/helpers"
)

/**
* all admin modules route will store here
 */
func Routes(r *gin.RouterGroup) *gin.RouterGroup {
	/// init project
	helpers.GET(r, Init)

	return r
}
